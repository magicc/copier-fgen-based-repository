# Copier - fgen-based repository

<!--- sec-begin-status -->

Status: new project, check back in later

<!--- sec-end-status -->

<!--- sec-begin-description -->

This is our copier template for repositories that use [fgen](https://gitlab.com/magicc/fgen) to wrap Fortran code.
It is built to work with [copier](https://copier.readthedocs.io/en/stable/#quick-start).

The template itself lives in `template`.


## Supported features

We support generating code for GitLab and GitHub-based projects.
The resulting generated code differs slightly
depending on whether GitLab or GitHub is targeted
(the `project_url` copier variable is used to determine which template to use).
The table below shows the different features for GitLab or GitHub projects:

|                                                 | GitLab | GitHub |
|-------------------------------------------------|--------|--------|
| Autogeneration of Fortran Wrappers using fgen   | y      | y      |
| CMake/scikit-build-core based build system      | y      | y      |
| CI                                              | n      | y      |
| Build wheels for different OS/CPU Architectures | n      | y      |
| Automatically publish releases to PyPi          | n      | y      |

We recommend using GitHub if you wish to publish the project
because of the automated wheel generation during the release process.

<!--- sec-end-description -->

## Installation

<!--- sec-begin-installation -->

It is expected that `copier` is installed globally. `pipx` can be used to install `copier` in it's own isolated
environment.

```
pipx install copier
```

<!--- sec-end-installation -->

## Development

For development, we rely on [poetry](https://python-poetry.org) for installing all our
development dependencies. To get started, you will need to make sure that poetry
is installed
([instructions here](https://python-poetry.org/docs/#installing-with-the-official-installer),
we found that pipx and pip worked better to install on a Mac).

For all of work, we use our `Makefile`.
You can read the instructions out and run the commands by hand if you wish,
but we generally discourage this because it can be error prone.
In order to create your environment, run `make virtual-environment`.

If there are any issues, the messages from the `Makefile` should guide you
through. If not, please raise an issue in the
[issue tracker](https://gitlab.com/magicc/copier-fgen-based-repository/-/issues).

### Copier template tester (ctt)

We use [copier template tester (ctt)](https://copier-template-tester.kyleking.me/)
to generate the output of using our template. This output is stored in the
`tests/regression/ctt` folder which is tracked by git and automatically updated by our
pre-commit hooks. This folder provides a way for us to easily see the impact
that changes to our template have on generated repositories under different
possible answers to our copier questions. Put another way, ctt provides a pure
regression test of our template, making sure that any changes to the output it
generates are immediately obvious and trackable over different commits.
