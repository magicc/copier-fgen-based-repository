Added support for building target packages in a single build step. This includes:
* Fetching and building fgen
* Generating wrapped modules via CMakeFiles
* Building the python package
