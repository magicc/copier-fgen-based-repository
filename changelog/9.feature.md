Add GitHub CI workflow. This includes workflows which cover the following:

- Linting
- Testing
- Cross-platform wheel generation
- Bumping new versions
- Generating a GitHub release
- Publishing wheels to PyPi on new releases
