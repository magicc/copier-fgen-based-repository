(development-reference)=
# Development

Notes for developers. If you want to get involved, please do!

## Language

We use British English for our development.
We do this for consistency with the broader work context of our lead developers.

## Versioning

This package follows the version format described in [PEP440](https://peps.python.org/pep-0440/) and
[Semantic Versioning](https://semver.org/) to describe how the version should change depending on the updates to the
code base. Our commit messages are written using written to follow the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) standard which makes it easy to find the
commits that matter when traversing through the commit history.

(releasing-reference)=
## Releasing

Releasing is semi-automated via a CI job. The CI job requires the type of version bump that will be performed to be
manually specified. See the poetry docs for the [list of available bump rules](https://python-poetry.org/docs/cli/#version).

### Standard process

The steps required are the following:

1. Bump the version: manually trigger the "bump" stage from the latest commit
   in main (pipelines are [here](https://gitlab.com/magicc/copier-fgen-based-repository/-/pipelines)).
   A valid "bump_rule" (see https://python-poetry.org/docs/cli/#version)
   will need to be specified via the "BUMP_RULE" CI
   variable (see https://docs.gitlab.com/ee/ci/variables/). This will then
   trigger a release, including publication to PyPI.

1. [Update projects](updating-repositories) to use the new copier version

## Read the Docs

We have not yet deployed on RtD, but this is on the to-do list.
