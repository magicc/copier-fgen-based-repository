# Getting Started

## Creating repositories

### First setup

If you're starting a new repository, go to that repository and then run
`copier copy --trust gl:magicc/copier-fgen-based-repository path/to/destination/repository`.
Copier will ask you the required questions and then setup your repository for you.

Alternatively, if you have the template repository cloned locally, the path to the
repository can be used instead of `gl:magicc/copier-fgen-based-repository`.

Copier will use the latest tagged release for generating a new project. If you wish to
use a specific commit/tag the `--vcs-ref` flag can be used (`--vcs-ref HEAD` will use
the most recent commit).

To run this step, you will need to have copier globally installed (we can't
make the virtual environment first as that is done with the `Makefile`, which
copier creates for us).

Once you have created your repository, there are then a number of further
steps which have to be done to get everything running as intended. The
sections below go through these steps.

#### PyPI

To be written, we don't have a good strategy for deploying via PyPI yet.

#### GitLab

To be written, we currently don't automatically set-up CI etc. for you.

#### GitHub

To be written, we currently don't automatically set-up CI etc. for you.

#### Read the Docs

To be written, we currently don't automatically set-up RtD for you.

(updating-repositories)=
## Updating repositories

If you need to update your repository, simply navigate to your repository and
run `copier update`. If you don't want to go through all the questions again
(the default answers are taken from last time you answered the questions), use
`copier --force update` instead.

By default, copier will use the most recent tag when updating the repository. If
you wish to use the current HEAD commit for your update, run
`copier --vcs-ref=HEAD update`. This `--vcs-ref` option can also be used to specify
a specific tag to apply.

When you update, there will likely be merge conflicts, particularly in
`pyproject.toml` related to versions. If you use the `--conflict inline`
option with `copier update` then the diffs should be inline (this is
apparently experimental though, see
[here](https://copier.readthedocs.io/en/stable/updating/)). These diffs can
then be resolved manually (how to automate this is an ongoing question).
The pre-commit config will make sure you don't miss conflicts and accidentally
commit merge conflict lines.

Any conflicts related to the `poetry.lock` file can be safety ignored and a `poetry lock`
should be run after updating to regenerate the lockfile.
