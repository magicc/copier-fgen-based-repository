# Copier - fgen based repository

```{include} ../../README.md
:start-after: <!--- sec-begin-description -->
:end-before: <!--- sec-end-description -->
```

## Installation

```{include} ../../README.md
:start-after: <!--- sec-begin-installation -->
:end-before: <!--- sec-end-installation -->
```

```{toctree}
:caption: Contents
:maxdepth: 2
getting_started
development
changelog
```

Index
-----

- :ref:`genindex`
- :ref:`modindex`
- :ref:`search`
