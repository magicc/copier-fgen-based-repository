"""
Copier template for fgen-based repositories

Placeholder package for versioning. The real work happens in `/template`.
"""
import importlib.metadata

__version__ = importlib.metadata.version("copier-fgen-based-repository")
