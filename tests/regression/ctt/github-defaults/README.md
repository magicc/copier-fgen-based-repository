# CTT Project

<!---
Can use start-after and end-before directives in docs, see
https://myst-parser.readthedocs.io/en/latest/syntax/organising_content.html#inserting-other-documents-directly-into-the-current-document
-->

<!--- sec-begin-description -->

Test project made with copier-template-tester



[![CI](https://github.com/climate-resource/ctt-project/actions/workflows/ci.yaml/badge.svg?branch=main)](https://github.com/climate-resource/ctt-project/actions/workflows/ci.yaml)
[![Coverage](https://codecov.io/gh/climate-resource/ctt-project/branch/main/graph/badge.svg)](https://codecov.io/gh/climate-resource/ctt-project)
[![Docs](https://readthedocs.org/projects/ctt-project/badge/?version=latest)](https://ctt-project.readthedocs.io)

**PyPI :**
[![PyPI](https://img.shields.io/pypi/v/ctt-project.svg)](https://pypi.org/project/ctt-project/)
[![PyPI: Supported Python versions](https://img.shields.io/pypi/pyversions/ctt-project.svg)](https://pypi.org/project/ctt-project/)
[![PyPI install](https://github.com/climate-resource/ctt-project/actions/workflows/install.yaml/badge.svg?branch=main)](https://github.com/climate-resource/ctt-project/actions/workflows/install.yaml)

**Other info :**
[![License](https://img.shields.io/github/license/climate-resource/ctt-project.svg)](https://github.com/climate-resource/ctt-project/blob/main/LICENSE)
[![Last Commit](https://img.shields.io/github/last-commit/climate-resource/ctt-project.svg)](https://github.com/climate-resource/ctt-project/commits/main)
[![Contributors](https://img.shields.io/github/contributors/climate-resource/ctt-project.svg)](https://github.com/climate-resource/ctt-project/graphs/contributors)


<!--- sec-end-description -->

Full documentation can be found at:
[ctt-project.readthedocs.io](https://ctt-project.readthedocs.io/en/latest/).
We recommend reading the docs there because the internal documentation links
don't render correctly on GitHub's viewer.

## Installation

<!--- sec-begin-installation -->

TODO: set up this part of the workflow and test it (https://gitlab.com/magicc/copier-fgen-based-repository/-/issues/5)

CTT Project can be installed with conda or pip:

```bash
pip install ctt-project
conda install -c conda-forge ctt-project
```


<!--- sec-end-installation -->

### For developers

<!--- sec-begin-installation-dev -->

```sh
make virtual-environment
make fgen-wrappers
make build-fgen
make install
make test
```

TODO: update this because we have non-Python dependencies (related to https://gitlab.com/magicc/copier-fgen-based-repository/-/issues/6)

For development, we rely on [poetry](https://python-poetry.org) for all our
dependency management. To get started, you will need to make sure that poetry
is installed
([instructions here](https://python-poetry.org/docs/#installing-with-the-official-installer),
we found that pipx and pip worked better to install on a Mac).

For all of our work, we use our `Makefile`.
You can read the instructions out and run the commands by hand if you wish,
but we generally discourage this because it can be error prone.
In order to create your environment, run `make virtual-environment`.

If there are any issues, the messages from the `Makefile` should guide you
through. If not, please raise an issue in the [issue tracker][issue_tracker].

For the rest of our developer docs, please see [](development-reference).

[issue_tracker]: https://github.com/climate-resource/ctt-project/issues

<!--- sec-end-installation-dev -->
