ctt\_project.derived\_type
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ctt_project.derived_type

.. currentmodule:: ctt_project.derived_type



DerivedType
===========

.. autoclass:: DerivedType
   :members:


DerivedTypeNoSetters
====================

.. autoclass:: DerivedTypeNoSetters
   :members:


DerivedTypeContext
==================

.. autoclass:: DerivedTypeContext
   :members:


DerivedTypeNoSettersContext
===========================

.. autoclass:: DerivedTypeNoSettersContext
   :members:
