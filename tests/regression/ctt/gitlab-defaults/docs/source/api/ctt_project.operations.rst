ctt\_project.operations
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: ctt_project.operations

.. currentmodule:: ctt_project.operations



Operator
========

.. autoclass:: Operator
   :members:


OperatorNoSetters
=================

.. autoclass:: OperatorNoSetters
   :members:


OperatorContext
===============

.. autoclass:: OperatorContext
   :members:


OperatorNoSettersContext
========================

.. autoclass:: OperatorNoSettersContext
   :members:
