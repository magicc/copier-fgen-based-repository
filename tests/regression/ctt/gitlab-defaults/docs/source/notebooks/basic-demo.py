# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Basic demo
#
# This notebook gives a basic demonstration of how to use CTT Project.

# %%
import pint

# Sometimes, we have found that we have to
# set the application registry before importing other packages to ensure
# that the unit registry is set correctly.
# This can happen because of an issue with pint's/`fgen_runtime.verify_unit`'s way of
# doing the wrapping which we haven't tried to tackle in a neater way yet.
# The code you will need is something like the below
# (but you would want to use your registry, e.g. ``openscm_units``,
# rather than pint's default registry).

# my_unit_registry = pint.UnitRegistry
# # Or, e.g. ``my_unit_registry = openscm_units.unit_registry``
# pint.set_application_registry(pint.UnitRegistry)

Q = pint.get_application_registry().Quantity

# %%
import ctt_project
from ctt_project.derived_type import DerivedType

# %%
print(f"You are using ctt_project version {ctt_project.__version__}")

# %% [markdown]
# The auto-generated Python wrappers give Python access to derived types defined in Fortran.

# %%
dt = DerivedType.from_build_args(base=Q(2, "m"))
dt

# %%
dt.base

# %%
dt.add(Q(3, "cm"))
