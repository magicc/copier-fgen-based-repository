"""
Test project made with copier-template-tester
"""
import importlib.metadata

__version__ = importlib.metadata.version("ctt_project")
