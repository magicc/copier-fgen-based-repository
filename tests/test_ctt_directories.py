"""
Test directories made with copier-template-tester (ctt)

In theory, these tests should only be run after we have run ctt to ensure the
latest changes are picked up. In practice, even if you forget to run ctt the
CI will make sure you don't miss things completely.
"""
import os
import subprocess
from pathlib import Path

import pytest

CTT_DIR = Path(__file__).parent / "regression" / "ctt"

ctt_directories = pytest.mark.parametrize(
    "ctt_dir", [pytest.param(CTT_DIR.absolute() / d, id=d) for d in os.listdir(CTT_DIR)]
)


def check_ctt_dir_in_output(ctt_dir, output):
    assert str(ctt_dir / ".venv") in output, "venv set incorrectly"


def setup_venv(ctt_dir, env):
    # This also does the build. I am tempted to pull that out, but you need the package
    # installed for lots of things to work too (and we install the package as part of
    # our usual make virtual-environment calls so I think the way it is currently done
    # is the more standard pattern).
    try:
        del env["VIRTUAL_ENV"]
    except KeyError:
        pass

    # I can't remember why I used the make commands here and the poetry commands below,
    # should probably unify...
    subprocess.run(("make", "virtual-environment"), cwd=ctt_dir, env=env, check=False)

    res_venv = subprocess.run(
        ("poetry", "env", "info"), cwd=ctt_dir, env=env, stdout=subprocess.PIPE, check=False
    )
    check_ctt_dir_in_output(ctt_dir, res_venv.stdout.decode())


def assert_tool_in_venv(tool, ctt_dir, env):
    res = subprocess.run(
        ("poetry", "run", "which", tool), cwd=ctt_dir, env=env, stdout=subprocess.PIPE, check=False
    )

    check_ctt_dir_in_output(ctt_dir, res.stdout.decode())


@ctt_directories
def test_docs_build(ctt_dir):
    env = os.environ
    setup_venv(ctt_dir, env)
    assert_tool_in_venv("sphinx-build", ctt_dir, env)

    if "no-initial-setup" in ctt_dir.name:
        shouldnt_exist = ["index.md", "notebooks.md"]
        for file in shouldnt_exist:
            assert not (ctt_dir / "docs" / "source" / file).exists()

    else:
        res = subprocess.run(
            (
                "poetry",
                "run",
                "sphinx-build",
                "-E",
                "-W",
                "-b",
                "html",
                "docs/source",
                "docs/build",
            ),
            cwd=ctt_dir,
            env=env,
            check=False,
        )
        res.check_returncode()


@ctt_directories
def test_liccheck_available(ctt_dir, tmp_path):
    # the base setup has no dependencies so all we are really doing here is
    # checking that liccheck is installed which is ok
    env = os.environ
    setup_venv(ctt_dir, env)
    assert_tool_in_venv("liccheck", ctt_dir, env)

    res = subprocess.run(
        (
            "poetry",
            "export",
            "--without=tests",
            "--without=docs",
            "--without=dev",
        ),
        cwd=ctt_dir,
        env=env,
        stdout=subprocess.PIPE,
        check=False,
    )
    export_file = tmp_path / "requirements.txt"
    with open(export_file, "w") as fh:
        fh.write(res.stdout.decode())

    res = subprocess.run(
        (
            "poetry",
            "run",
            "liccheck",
            "-r",
            str(export_file.absolute()),
        ),
        cwd=ctt_dir,
        env=env,
        stdout=subprocess.PIPE,
        check=False,
    )

    res.check_returncode()
    assert "forbidden" not in res.stdout.decode()
    assert "unknown" not in res.stdout.decode()


@ctt_directories
def test_tests_pass(ctt_dir):
    env = os.environ
    setup_venv(ctt_dir, env)

    assert_tool_in_venv("pytest", ctt_dir, env)
    res = subprocess.run(
        (
            "poetry",
            "run",
            "pytest",
            "src",
            "tests",
            "-r",
            "a",
            "-v",
            "--doctest-modules",
            # # Not sure how to have cov here without hard-coding project name
            # "--cov",
            # "ctt_project",
        ),
        cwd=ctt_dir,
        env=env,
        check=False,
    )
    res.check_returncode()


@pytest.mark.xfail(reason="Generated Fortran isn't formatted, see https://gitlab.com/magicc/fgen/-/issues/26")
@ctt_directories
def test_pre_commit(ctt_dir):
    env = os.environ
    setup_venv(ctt_dir, env)
    assert_tool_in_venv("pre-commit", ctt_dir, env)

    res = subprocess.run(
        (
            "poetry",
            "run",
            "pre-commit",
            "run",
            "--all-files",
        ),
        cwd=ctt_dir,
        env=env,
        check=False,
    )

    res.check_returncode()


@ctt_directories
def test_mypy(ctt_dir):
    env = os.environ
    setup_venv(ctt_dir, env)
    assert_tool_in_venv("mypy", ctt_dir, env)

    res = subprocess.run(
        (
            "poetry",
            "run",
            "mypy",
            "src",
        ),
        cwd=ctt_dir,
        env=env,
        check=False,
    )

    res.check_returncode()


@ctt_directories
def test_cmakelists_formatted(ctt_dir):
    env = os.environ
    setup_venv(ctt_dir, env)
    assert_tool_in_venv("cmake-lint", ctt_dir, env)

    res = subprocess.run(
        (
            "poetry",
            "run",
            "cmake-lint",
            "CMakeLists.txt",
            "-c",
            ".cmakelang.yaml",
        ),
        cwd=ctt_dir,
        env=env,
        check=False,
    )

    res.check_returncode()
